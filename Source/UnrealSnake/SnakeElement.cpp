// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeElement.h"


// Sets default values
ASnakeElement::ASnakeElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	/*
	//Mesh Component setup
	snakeElementMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	snakeElementMesh->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> PlayerMeshAsset(TEXT("/Engine/BasicShapes/Cube.Cube"));

	if (PlayerMeshAsset.Succeeded())
	{
		snakeElementMesh->SetStaticMesh(PlayerMeshAsset.Object);
		snakeElementMesh->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
		//SnakeMesh->SetWorldScale3D(FVector(0.3f, 0.3f, 0.4f));
	}
	snakeElementMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -40.0f));
	//snakeElementMesh->CastShadow = false;
	*/
	//Material setups for mesh
	snakeElementMaterial01 = CreateDefaultSubobject<UMaterial>(TEXT("Snake Element Material01"));
	snakeElementMaterial02 = CreateDefaultSubobject<UMaterial>(TEXT("Snake Element Material02"));
	static ConstructorHelpers::FObjectFinder<UMaterial> snakeElementMeshMaterial01(TEXT("Material'/Game/SnakeElement01.SnakeElement01'"));
	static ConstructorHelpers::FObjectFinder<UMaterial> snakeElementMeshMaterial02(TEXT("Material'/Game/SnakeElement02.SnakeElement02'"));

	if (snakeElementMeshMaterial01.Succeeded())
	{
		//snakeElementMesh->SetMaterial(0, snakeElementMeshMaterial01.Object);

		snakeElementMaterial01 = snakeElementMeshMaterial01.Object;
	}

	if (snakeElementMeshMaterial02.Succeeded())
	{
		//snakeElementMesh->SetMaterial(0, snakeElementMeshMaterial01.Object);

		snakeElementMaterial02 = snakeElementMeshMaterial02.Object;
	}

	//snakeElementMesh->SetMaterial(0, snakeElementMaterial01);
	//destructableMesh->SetMaterial(0, snakeElementMaterial01);
	
	destructableMesh = CreateDefaultSubobject<UDestructibleComponent>(TEXT("Destructable Mesh"));
	destructableMesh->SetupAttachment(RootComponent);
	destructableMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -40.0f));
	destructableMesh->SetSimulatePhysics(true);
	destructableMesh->CastShadow = false;

	static ConstructorHelpers::FObjectFinder<UDestructibleMesh> DestructableMeshAsset(TEXT("DestructibleMesh'/Game/Cube_DM.Cube_DM'"));

	if (DestructableMeshAsset.Succeeded())
	{
		destructableMesh->SetDestructibleMesh(DestructableMeshAsset.Object);
	}
	
}


void ASnakeElement::DestroySnakeElement()
{
	UE_LOG(LogTemp, Warning, TEXT("destroying snake element:"));
	//this->Destroy();
	destructableMesh->ApplyDamage(12000, this->GetActorLocation(), GetActorForwardVector(), 12000);
}

void ASnakeElement::FlippSize(bool state)
{
	if (state == true)
	{
		//snakeElementMesh->SetWorldScale3D(FVector(1.3f, 1.3f, 1.0f));
		//snakeElementMesh->SetMaterial(0, snakeElementMaterial02);
		destructableMesh->SetWorldScale3D(FVector(1.3f, 1.3f, 1.0f));
		//destructableMesh->SetMaterial(0, snakeElementMaterial02);

	}
	else
	{
		//snakeElementMesh->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
		//snakeElementMesh->SetMaterial(0, snakeElementMaterial01);
		destructableMesh->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
		destructableMesh->SetMaterial(0, snakeElementMaterial01);
	}
}

// Called when the game starts or when spawned
void ASnakeElement::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASnakeElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

