// Krystian Slawinski

#pragma once
#include "Particles/ParticleSystem.h"
#include "CoreMinimal.h"
#include "Pickup.h"
#include "IcePickup.generated.h"

/**
 * 
 */
UCLASS()
class UNREALSNAKE_API AIcePickup : public APickup
{
	GENERATED_BODY()

public:
	AIcePickup();

	virtual void DestroyPickup(AActor* instigator) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Atributes")
	UParticleSystem* explosionParticle;
	
	
};
