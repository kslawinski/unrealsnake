// Krystian Slawinski

#pragma once

#include "Components/StaticMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/DestructibleComponent.h"


#include "Materials/Material.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

UCLASS()
class UNREALSNAKE_API APickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickup();

	virtual void DestroyPickup(AActor* instigator);

UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SETUP")
class UDestructibleComponent* destructableMesh;
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DEBUG")
class AUnrealSnakeGameModeBase* gameMode;

UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DEBUG")
bool hasDied = false;
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DEBUG")
float defCounter = 0;

UParticleSystemComponent* particleComponent;
class UParticleSystem* destroyParticle;
UStaticMeshComponent* pickupMesh;

UMaterialInterface* destructableMeshMaterial;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void BeginDestroy() override;
	void PlayParticle(class UParticleSystem* particleToPlay);


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:


	float defCounterSave = 10;



	
};
