// Krystian Slawinski

#pragma once
#include "UObject/ConstructorHelpers.h"
#include "Particles/ParticleSystemComponent.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Affector.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UNREALSNAKE_API UAffector : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAffector();
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Atributes")
	UParticleSystemComponent* particleComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Atributes")
	UParticleSystem* affectorParticle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Atributes")
	AActor* actorToAffect;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Atributes")
	float affectTimeSave = 3.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float affectCounter = 0.0f;
	float affectorLifeTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float affectorLifeCounter;


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void InitializeAffector(USceneComponent* root);
	
};
