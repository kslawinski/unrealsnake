// Krystian Slawinski

#include "IcePickup.h"
#include "IceAffector.h"
#include "engine/DestructibleMesh.h"
AIcePickup::AIcePickup()
{
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ExplosionParticleAsset(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));

	if (ExplosionParticleAsset.Succeeded())
	{
		explosionParticle = ExplosionParticleAsset.Object;
	}

	static ConstructorHelpers::FObjectFinder<UDestructibleMesh> DestructableMeshAssetr(TEXT("DestructibleMesh'/Game/IcePickup_DM1.IcePickup_DM1'"));

	if (DestructableMeshAssetr.Succeeded())
	{
		destructableMesh->SetDestructibleMesh(DestructableMeshAssetr.Object);
	}
}

void AIcePickup::DestroyPickup(AActor * instigator)
{
	UPrimitiveComponent* instigatorRoot = instigator->GetRootPrimitiveComponent();

	//UAffector* affector = CreateDefaultSubobject<UAffector>(TEXT("AffectorComp"));

	//CompClass can be a BP
	//USceneComponent* NewComp = ConstructObject<USceneComponent>(UAffector::StaticClass(), instigator, FName("Affector Component"));

	UIceAffector* NewComp;

	NewComp = NewObject<UIceAffector>(instigator->GetRootComponent(), FName("Ice Affector Component"), RF_NoFlags);
	NewComp->RegisterComponent();
	particleComponent->RegisterComponent();
	NewComp->InitializeAffector(instigator->GetRootComponent());


	APickup::DestroyPickup(instigator);
}
