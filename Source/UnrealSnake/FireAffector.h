// Krystian Slawinski

#pragma once

#include "CoreMinimal.h"
#include "Affector.h"
#include "FireAffector.generated.h"

/**
 * 
 */
UCLASS()
class UNREALSNAKE_API UFireAffector : public UAffector
{
	GENERATED_BODY()
public:
	UFireAffector();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	virtual void InitializeAffector(USceneComponent* root) override;

	virtual void DestroyComponent(bool bPromoteChildren) override;
};
