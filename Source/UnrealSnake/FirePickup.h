// Krystian Slawinski

#pragma once
#include "Particles/ParticleSystem.h"
#include "CoreMinimal.h"
#include "Pickup.h"
#include "FirePickup.generated.h"

/**
 * 
 */
UCLASS()
class UNREALSNAKE_API AFirePickup : public APickup
{
	GENERATED_BODY()
	
public:
	AFirePickup();

	virtual void DestroyPickup(AActor* instigator) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Atributes")
	UParticleSystem* explosionParticle;
	
};
