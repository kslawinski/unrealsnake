// Krystian Slawinski

#include "IceAffector.h"
#include "Snake.h"

UIceAffector::UIceAffector()
{
	static ConstructorHelpers::FObjectFinder<UParticleSystem> FireParticleAsset(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Ice.P_Ice'"));

	if (FireParticleAsset.Succeeded())
	{
		affectorParticle = FireParticleAsset.Object;
	}
}

void UIceAffector::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (affectorLifeCounter <= 0)
	{
		ASnake* snake = Cast<ASnake>(actorToAffect);

		if (!snake->isDead)
		{
			affectorLifeCounter = affectorLifeTime;
			// remove affector particle
			if (particleComponent != nullptr)
				if (particleComponent->IsPendingKill() == false)
					particleComponent->DestroyComponent();
		}
		// kill affector
		this->DestroyComponent(false);
	}

	if (actorToAffect)
	{
		affectCounter -= DeltaTime;

		if (affectCounter <= 0)
		{
			//reset counter
			affectCounter = affectTimeSave;

			// take damage maybe
			FDamageEvent damageEvent;
			APawn* controller = Cast<APawn>(actorToAffect);


			//actorToAffect->TakeDamage(20.0f, damageEvent, controller->GetController(), NULL);
		}
	}
}

void UIceAffector::InitializeAffector(USceneComponent * root)
{
	Super::InitializeAffector(root);
	ASnake* snake = Cast<ASnake>(actorToAffect);

	if (snake)
		snake->snakeSpeed /= 2;
}

void UIceAffector::DestroyComponent(bool bPromoteChildren)
{
	Super::DestroyComponent(bPromoteChildren);

	ASnake* snake = Cast<ASnake>(actorToAffect);
	if (snake)
		snake->snakeSpeed = 4;
}
