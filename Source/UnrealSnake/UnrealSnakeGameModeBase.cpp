// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealSnakeGameModeBase.h"
#include "Snake.h"
#include "Pickup.h"
#include "FirePickup.h"
#include "IcePickup.h"
#include "SnakeElement.h"
#include "GameUI.h"
#include "WallElement.h"


AUnrealSnakeGameModeBase::AUnrealSnakeGameModeBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AUnrealSnakeGameModeBase::SpawnPickup()
{
	// GET RANDOM POSITION X Y
	int32 ranioX = FMath::FRandRange(-9, 9);
	int32 randX = (ranioX * 100); // usunac ranio
	UE_LOG(LogTemp, Warning, TEXT("RANDOM X: %d"), ranioX);
	UE_LOG(LogTemp, Warning, TEXT("RANDOM X razy 100: %d"), randX);

	int32 ranioY = FMath::FRandRange(-19, 19);
	int32 randY = ( ranioY * 100);
	FVector randomSpawnPos1 = FVector(randX,randY,0);


	//TODO SPALNI DWA W JEDNYM MIESJSCU FIX THIS

	FActorSpawnParameters SpawnInfo;
	// if time for special pickup spawn it
	if (specialPickupSpawnCounter <= 0 && currentSpecialPickup == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("spawning FirePickup: %f"), 1.0f);
		specialPickupSpawnCounter = specialPickupSpawnCounterSave;
		specialPickupCounter = specialPickupCounterSave;

		int32 randomSpawn = FMath::FRandRange(0, 10);

		if (randomSpawn >= 0 && randomSpawn <= 3)
		{
			AFirePickup* FirePickupInstance = GWorld->SpawnActor<AFirePickup>(randomSpawnPos1, FRotator(0, 0, 0), SpawnInfo);
			FirePickupInstance->gameMode = this;
			// ADD IT TO LIST FOR FUTURE REF
			gameObjectsList.Add(FirePickupInstance);
			currentSpecialPickup = FirePickupInstance;
		}
		else
		{
			if (currentSpecialPickup == nullptr)
			{
				AIcePickup* IcePickupInstance = GWorld->SpawnActor<AIcePickup>(randomSpawnPos1, FRotator(0, 0, 0), SpawnInfo);
				IcePickupInstance->gameMode = this;
				// ADD IT TO LIST FOR FUTURE REF
				gameObjectsList.Add(IcePickupInstance);
				currentSpecialPickup = IcePickupInstance;
			}
		}
	}

	if (currentPickup == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("spawning Pickup: %f"), 1.0f);
		//SPAWN PICKUP
		int32 ranioX2 = FMath::FRandRange(-6, 6);
		int32 randX2 = (ranioX2 * 100); // usunac ranio
		UE_LOG(LogTemp, Warning, TEXT("RANDOM X: %d"), ranioX2);
		UE_LOG(LogTemp, Warning, TEXT("RANDOM X razy 100: %d"), randX2);

		int32 ranioY2 = FMath::FRandRange(-18, 18);
		int32 randY2 = (ranioY * 100);
		FVector randomSpawnPos2 = FVector(randX2, randY2, 0);

		APickup* PickupInstance = GWorld->SpawnActor<APickup>(randomSpawnPos2, FRotator(0, 0, 0), SpawnInfo);
		PickupInstance->gameMode = this;
		// ADD IT TO LIST FOR FUTURE REF
		gameObjectsList.Add(PickupInstance);
		currentPickup = PickupInstance;
	}
}

void AUnrealSnakeGameModeBase::GenerateMap()
{
	int32 mapMinX = -1900;
	int32 mapMinY = -900;

	int32 mapMaxTilesX = 40;
	int32 mapMaxTilesY = 25;

	for (int32 i = 0; i < mapMaxTilesX; i++)
	{
		for (int32 j = 0; j < mapMaxTilesY; j++)
		{
			if ((j == 0 || j == mapMaxTilesY -1) || (i == 0 || i == mapMaxTilesX-1))
			{
				FActorSpawnParameters SpawnInfo;
				FVector spawnPos = FVector(mapMinY + j * 100, mapMinX + i * 100, 40);
				AWallElement* wallInstance = GWorld->SpawnActor<AWallElement>(spawnPos, FRotator(0, 0, 0), SpawnInfo);
				gameObjectsList.Add(wallInstance);
			}
		}
	}
}

void AUnrealSnakeGameModeBase::RestartMap()
{
	snakeInstance->ResetSnake();
	snakeInstance->Destroy();
	snakeInstance = nullptr;


	gameOver = false;
	gameObjectsList.Reset(0);

	moveTimer = moveTimerSave;

	playerScore = 0;

	//RestartPlayer(playerController);

	//GenerateMap();
}

FString AUnrealSnakeGameModeBase::GetSpecialPickupRemainingTimeString()
{

	FString textToDisplay = FString("S:");

	if (specialPickupCounter >= 0)
	{
		textToDisplay.AppendInt(StaticCast<int>(this->specialPickupCounter));
	}

	return textToDisplay;
}

void AUnrealSnakeGameModeBase::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage)
{
	Super::InitGame(MapName,Options,ErrorMessage);
	UE_LOG(LogTemp, Warning, TEXT("INIT GAME: %f"), 1.0f);

	playerScore = 0;

	for (TActorIterator<AActor> actorvalue(GetWorld()); actorvalue; ++actorvalue)
	{

		if (*actorvalue != nullptr)
		{
			if (actorvalue->GetName().Contains("GameUI"))
			{
				gameUI = Cast<AGameUI>(*actorvalue);
				continue;
			}
		}
	}

	//Find the main camera
	for (TActorIterator<AActor> actorvalue(GetWorld()); actorvalue; ++actorvalue)
	{
		if (*actorvalue != nullptr)
		{
			if (actorvalue->GetName().Contains("MainCamera"))
				mainCameraActor = *actorvalue;
		}
	}



	if (mainCameraActor != nullptr)
		if (playerController)
		{
			playerController->SetViewTargetWithBlend(mainCameraActor); // set the view target to main camera
			playerController->bShowMouseCursor = false;
		}

	gameUIWidgetInstance = CreateWidget<UGameUIWidget>(GetWorld(), UIClass);

	if (gameUIWidgetInstance != nullptr)
	{
		gameUIWidgetInstance->AddToViewport(); // add widget to the screen
		gameUIWidgetInstance->InitUI(this);
	}

	moveTimer = moveTimerSave;
	//specialPickupCounter = specialPickupCounterSave;
	specialPickupSpawnCounter = specialPickupSpawnCounterSave;

	ESlateVisibility visibility = ESlateVisibility::Hidden;
	gameUIWidgetInstance->SetVisibility(visibility);
}

void AUnrealSnakeGameModeBase::ShowPausePanel(bool status)
{
	if (status == true)
	{
		ESlateVisibility visibility = ESlateVisibility::Visible;
		gameUIWidgetInstance->SetVisibility(visibility);
		if (playerController)
		{
			playerController->bShowMouseCursor = true;
			playerController->bEnableMouseOverEvents = true;
		}
	}
	else
	{
		ESlateVisibility visibility = ESlateVisibility::Hidden;
		gameUIWidgetInstance->SetVisibility(visibility);
		if (playerController)
		{
			playerController->bShowMouseCursor = false;
			playerController->bEnableMouseOverEvents = false;
		}
	}
}

// Called every frame
void AUnrealSnakeGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//UE_LOG(LogTemp, Warning, TEXT("Ticking: %f"), 1.0f);
	if (snakeInstance->isDead)
		gameOver = true;
	if (gameOver) 
	{
		//snakeInstance->SelfDestruction(DeltaTime);

		if (gameUIWidgetInstance->GetVisibility() == ESlateVisibility::Hidden)
		{
			ShowPausePanel(true);
			snakeInstance->isDead = true;
		}

		return;
	} // handle game over state // TODO Add enums for game states

	if(snakeInstance)
	moveTimer -= DeltaTime * snakeInstance->snakeSpeed;

	if (moveTimer <= 0.0f)
	{
		moveTimer = moveTimerSave;

		if (gameUI)
		{
			gameUI->uIScoreText->SetText(FString::FromInt(playerScore));
		}
		// move snake
		if (snakeInstance)
			snakeInstance->MoveSnake();

		if(gameObjectsList.Num() >0)
			for (int32 i = 0; i< gameObjectsList.Num() ; i++)
			{
				// if colliding with pickups
				AActor* actor = gameObjectsList[i];

				if (actor->IsPendingKill()) { continue; }

				if (snakeInstance->CheckSnakeCollision(actor))
				{

					if (actor->GetName().Contains("FirePickup"))
					{
						UE_LOG(LogTemp, Warning, TEXT("colliding with FirePickup: %f"), 1.0f);
						AFirePickup* firePickup = Cast<AFirePickup>(actor);

						if (firePickup->hasDied) { return; }

						if (firePickup)
						{
							//specialPickupCounter = specialPickupCounterSave;
							snakeInstance->AddSnakeElement();
							snakeInstance->EatPickup(firePickup);
							specialPickupCounter = 0;
								SpawnPickup();

							//add score
							playerScore += 300;
							return;
						}
					}
					else if (actor->GetName().Contains("IcePickup"))
					{
						UE_LOG(LogTemp, Warning, TEXT("colliding with IcePickup: %f"), 1.0f);
						AIcePickup* icePickup = Cast<AIcePickup>(actor);

						if (icePickup->hasDied) { return; }

						if (icePickup)
						{
							//specialPickupCounter = specialPickupCounterSave;
							snakeInstance->AddSnakeElement();
							snakeInstance->EatPickup(icePickup);
							specialPickupCounter = 0;
							SpawnPickup();

							//add score
							playerScore += 300;
							return;
						}
					}
					else if (actor->GetName().Contains("Pickup"))
						{
							
							//actor->Destroy();
							APickup* pickup = Cast<APickup>(actor);

							if (pickup->hasDied) { return; }
							if (pickup)
							{
								//pickup->DestroyPickup();
								snakeInstance->AddSnakeElement();
								snakeInstance->EatPickup(pickup);
								currentPickup = nullptr;
								SpawnPickup();
								//add score
								playerScore += 100;
								UE_LOG(LogTemp, Warning, TEXT("colliding with Pickup foodCount: %d"), 1);
								return;
							}
						}


					if (actor->GetName().Contains("WallElement"))
					{
						snakeInstance->isDead = true;
						gameOver = true;
					}
				}
					//return;
			}

		// if colliding with snake element

			if (snakeInstance && snakeInstance->snakeElements.Num() > 0)
				for (ASnakeElement* snakeElement : snakeInstance->snakeElements)
				{
					if (snakeInstance->CheckSnakeCollision(snakeElement))
					{
						if (snakeElement->GetName().Contains("SnakeElement"))
						{

							//ASnakeElement* snakeElement = Cast<ASnakeElement>(snakeElement);
							if (snakeElement)
							{
								snakeInstance->isDead = true;
								gameOver = true;
							}

						}
					}
				}
	}

	if (specialPickupCounter > 0)
	{
		specialPickupCounter -= DeltaTime; // count time for special pickup to collect time
		gameUI->uIScoreText->SetText(GetSpecialPickupRemainingTimeString());
	}
	else if (specialPickupCounter <= 0)
	{
		if (currentSpecialPickup != nullptr)
		{
			gameObjectsList.Remove(currentSpecialPickup);
			currentSpecialPickup->Destroy();
			currentSpecialPickup = nullptr;
		}
	}



	specialPickupSpawnCounter -= DeltaTime; // count time for special pickup spawn

}

// Called when the game starts or when spawned
void AUnrealSnakeGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	playerController = static_cast<APlayerController*>(GWorld->GetFirstPlayerController()); // set player controller

	if (playerController)
		snakeInstance = Cast<ASnake>(playerController->GetPawn()); // set controlled snake

																   //if (gameUI)
	if (gameUI)
	{
		gameUI->uIScoreText->SetText(FString::FromInt(playerScore));
	}
	GenerateMap();
	SpawnPickup();

	//UE_LOG(LogTemp, Warning, TEXT("Begin Play: %f"), 1.0f);
}


