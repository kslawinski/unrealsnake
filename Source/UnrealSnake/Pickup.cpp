// Krystian Slawinski

#include "Pickup.h"
#include "Particles/ParticleSystem.h"
#include "UnrealSnakeGameModeBase.h"
#include "engine/DestructibleMesh.h"
#include "UObject/ConstructorHelpers.h"
// Sets default values
APickup::APickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	pickupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PickupMesh"));

	pickupMesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(TEXT("/Engine/BasicShapes/Sphere.Sphere"));

	if (MeshAsset.Succeeded())
	{
		pickupMesh->SetStaticMesh(MeshAsset.Object);
		pickupMesh->SetWorldScale3D(FVector(0.3f, 0.3f, 0.3f));
	}

	destructableMesh = CreateDefaultSubobject<UDestructibleComponent>(TEXT("Destructable Mesh"));
	destructableMesh->SetupAttachment(RootComponent);
	destructableMesh->SetSimulatePhysics(true);
	destructableMesh->CastShadow = false;

	static ConstructorHelpers::FObjectFinder<UDestructibleMesh> DestructableMeshAsset(TEXT("DestructibleMesh'/Game/Sphere_DM.Sphere_DM'"));

	if (DestructableMeshAsset.Succeeded())
	{
		destructableMesh->SetDestructibleMesh(DestructableMeshAsset.Object);
	}

	static ConstructorHelpers::FObjectFinder<UParticleSystem> ExplosionParticleAsset(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));

	if (ExplosionParticleAsset.Succeeded())
	{
		destroyParticle = ExplosionParticleAsset.Object;
	}

	particleComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle Component"));
	particleComponent->SetupAttachment(RootComponent);
	particleComponent->SetWorldScale3D(FVector(3.0f, 3.0f, 3.0f));

	particleComponent->bAutoActivate = false;

	
}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();
	defCounter = defCounterSave;
}

// Called every frame
void APickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (hasDied)
	{
		defCounter -= DeltaTime;
		SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z - defCounter / 10));

		if (defCounter < 0)
		{
			if (gameMode)
				gameMode->gameObjectsList.Remove(this);
			this->Destroy();
		}
	}
}

void APickup::BeginDestroy()
{
	
	Super::BeginDestroy();
	UE_LOG(LogTemp, Warning, TEXT("Begin destroy in pickup: %f"), 1.0f);

}

void APickup::PlayParticle(UParticleSystem * particleToPlay)
{
	if (particleToPlay == nullptr) { return; }

	particleComponent->SetTemplate(particleToPlay);
	particleComponent->Activate();
}

void APickup::DestroyPickup(AActor* instigator)
{
	destructableMesh->ApplyDamage(6000, this->GetActorLocation(), GetActorForwardVector(), 6000);
	hasDied = true;
	PlayParticle(destroyParticle);
}

