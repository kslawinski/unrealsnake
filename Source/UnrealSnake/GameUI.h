// Krystian Slawinski

#pragma once
#include "Components/StaticMeshComponent.h"
#include "Components/TextRenderComponent.h"
#include "UObject/ConstructorHelpers.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameUI.generated.h"

UCLASS()
class UNREALSNAKE_API AGameUI : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameUI();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SETUP")
	UStaticMeshComponent* uIMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SETUP")
	UTextRenderComponent* uIScoreText;
};
