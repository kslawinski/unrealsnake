// Krystian Slawinski

#include "GameUI.h"


// Sets default values
AGameUI::AGameUI()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	uIMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("UI Mesh"));
	uIMesh->SetupAttachment(RootComponent);

	uIScoreText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("Score Text render Component"));
	uIScoreText->SetupAttachment(uIMesh);
}

// Called when the game starts or when spawned
void AGameUI::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGameUI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

