// Krystian Slawinski
#include "GameUIWidget.h"
#include "UnrealSnakeGameModeBase.h"

void UGameUIWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (WidgetTree)
	{
		UPanelWidget* RootWidget = WidgetTree->ConstructWidget<UPanelWidget>(UPanelWidget::StaticClass(), TEXT("RootWidget"));
		// If GetRootWidget() is still null
		WidgetTree->RootWidget = RootWidget;
	}

	UPanelWidget* RootWidget = Cast<UPanelWidget>(GetRootWidget());

	gameOverText = WidgetTree->ConstructWidget<UTextBlock>(UTextBlock::StaticClass(),TEXT("Game Over Text")); // The second parameter is the name and is optional.
	RootWidget->AddChild(gameOverText);

	gameOverText->Text = FText::FromString(TEXT("Helo World"));
}

bool UGameUIWidget::RestartGame()
{
	gameMode->RestartMap();
	return true;
}

bool UGameUIWidget::ShowGameOverPanel()
{
	return false;
}

void UGameUIWidget::InitUI(AUnrealSnakeGameModeBase * GameMode)
{
	gameMode = GameMode;
}
