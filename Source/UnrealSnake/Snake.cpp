// Fill out your copyright notice in the Description page of Project Settings.

#include "Snake.h"
#include "SnakeElement.h"
#include "Pickup.h"
#include "UnrealSnakeGameModeBase.h"

// Sets default values
ASnake::ASnake()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	//Camera component setup
	PlayerCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	//PlayerCamera->SetupAttachment(RootComponent);
	//Values obtained from UPROPERTY testing
	PlayerCamera->SetRelativeLocation(FVector(-200.0f, 0.0f, 300.0f));
	PlayerCamera->SetRelativeRotation(FRotator(-55.0f, 0.0f, 0.0f));

	//Mesh Component setup
	SnakeMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	SnakeMesh->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> PlayerMeshAsset(TEXT("/Engine/BasicShapes/Cube.Cube"));

	if (PlayerMeshAsset.Succeeded())
	{
		SnakeMesh->SetStaticMesh(PlayerMeshAsset.Object);
		SnakeMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -40.0f));
		//SnakeMesh->SetWorldScale3D(FVector(0.3f, 0.3f, 0.4f));
	}

	//SnakeMesh->CastShadow = false;
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
	moveTimer = moveTimerSave;

	for (uint32 i = 0; i <2; i++)
	{
		AddSnakeElement();
	}

	// Set this pawn to be controlled by the lowest-numbered player
	AutoPossessPlayer = EAutoReceiveInput::Player0;
	snakeSpeed = 4;
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isDead)
	{
		SelfDestruction(DeltaTime);
	}
}

// Called to bind functionality to input
void ASnake::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Actions
	InputComponent->BindAction("UP", IE_Pressed, this, &ASnake::TurnUP);
	InputComponent->BindAction("DOWN", IE_Pressed, this, &ASnake::TurnDown);
	InputComponent->BindAction("LEFT", IE_Pressed, this, &ASnake::TurnLeft);
	InputComponent->BindAction("RIGHT", IE_Pressed, this, &ASnake::TurnRight);
}

float ASnake::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	health -= 10;

	if (health <= 0)
	{
		isDead = true;
	}

	return 0.0f;
}

void ASnake::MoveSnake()
{
	//snakeElements.Last()->SetActorLocation(this->GetActorLocation());
	if(eatenPickupsPositions.Num() >0)
		for (FVector pos : eatenPickupsPositions)
		{
			ResizeSnakeElementAtLocation(pos);
		}

	FVector lastPosition = this->GetActorLocation();

	if (snakeDirection == ESnakeDirectionEnum::RIGHT)
	{
		this->SetActorLocation(GetActorLocation() + GetActorRightVector() * 100.0f);
	}
	else if (snakeDirection == ESnakeDirectionEnum::UP)
	{
		this->SetActorLocation(GetActorLocation() + GetActorForwardVector() * 100.0f);
	}
	else if (snakeDirection == ESnakeDirectionEnum::DOWN)
	{
		this->SetActorLocation(GetActorLocation() + -GetActorForwardVector() * 100.0f);
	}
	else if (snakeDirection == ESnakeDirectionEnum::LEFT)
	{
		this->SetActorLocation(GetActorLocation() + -GetActorRightVector() * 100.0f);
	}

	readyToTurn = true;

	FVector tempPos;
	for (int i = 0; i < snakeElements.Num(); i++)
	{
		tempPos = snakeElements[i]->GetActorLocation();
		snakeElements[i]->SetActorLocation(lastPosition);
		lastPosition = tempPos;
	}
}

void ASnake::TurnRight()
{
	if (snakeDirection != ESnakeDirectionEnum::LEFT && readyToTurn)
	{
		snakeDirection = ESnakeDirectionEnum::RIGHT;
		lastSnakeDirection = ESnakeDirectionEnum::RIGHT;
		readyToTurn = false;
	}
}
void ASnake::TurnLeft()
{
	if (snakeDirection != ESnakeDirectionEnum::RIGHT  && readyToTurn)
	{
		snakeDirection = ESnakeDirectionEnum::LEFT;
		lastSnakeDirection = ESnakeDirectionEnum::LEFT;
		readyToTurn = false;
	}
}
void ASnake::TurnUP()
{
	if (snakeDirection != ESnakeDirectionEnum::DOWN  && readyToTurn)
	{
		snakeDirection = ESnakeDirectionEnum::UP;
		readyToTurn = false;
	}
}
void ASnake::TurnDown()
{
	if (snakeDirection != ESnakeDirectionEnum::UP  && readyToTurn)
	{
		snakeDirection = ESnakeDirectionEnum::DOWN;
		readyToTurn = false;
	}
}

void ASnake::AddSnakeElement()
{
	ASnakeElement* snakeElement = (ASnakeElement*)GWorld->SpawnActor(ASnakeElement::StaticClass());
	//instantiate new SnakeElement and add it to the list of snake elements for future reference
	if(snakeElements.Num()>0)
	snakeElement->SetActorLocation(snakeElements.Last()->GetActorLocation() + (snakeElements.Last()->GetActorForwardVector()* -100));
	snakeElements.Add(snakeElement);
	//snakeElement->SetActorLocation(this->GetActorLocation() + (this->GetActorForwardVector() * FVector(-100* snakeElements.Num(), 0, 0)));
	//snakeElement->SetActorLocation(snakeElements.Last()->GetActorLocation() + (snakeElements.Last()->GetActorForwardVector() * FVector(-100 * snakeElements.Num(), 0, 0)));


}

void ASnake::ResetSnake()
{
	for (ASnakeElement* snakeElement : snakeElements)
	{
		snakeElements.Remove(snakeElement);
		snakeElement->Destroy();
	}
	snakeElements.Empty();
	eatenPickupsPositions.Empty();

	//snakeElements.Add((ASnakeElement*)GWorld->SpawnActor(ASnakeElement::StaticClass()));
}

bool ASnake::CheckSnakeCollision(AActor * actor)
{
	if (actor == nullptr) { return false; }

	if (actor->IsPendingKill()) // safety check
	{
		return false;
	}

	bool isColliding = false;

	// If colliding with head
	if (this->GetActorLocation().X == actor->GetActorLocation().X && this->GetActorLocation().Y == actor->GetActorLocation().Y)
	{
		isColliding = true;
	}

	return isColliding;
}

bool ASnake::EatPickup(APickup * pickup)
{
	bool eatSucess = false;

	eatenPickupsPositions.Add(pickup->GetActorLocation()); // store pickup loacation
	pickup->DestroyPickup(this);

	return eatSucess;
}

void ASnake::ResizeSnakeElementAtLocation(FVector location)
{
	for (ASnakeElement* snakeElement : snakeElements)
	{
			if (snakeElement->GetActorLocation().X == location.X && snakeElement->GetActorLocation().Y == location.Y)
			{
				snakeElement->FlippSize(true);
			}
			else
			{
				snakeElement->FlippSize(false);
			}
	}
}

void ASnake::SelfDestruction(float deltaTime)
{
	destructionCounter -= deltaTime;

	if (destructionCounter < 0 && destructionIndex < snakeElements.Num())
	{
		destructionCounter = destructionCounterSave;

		snakeElements[destructionIndex]->DestroySnakeElement();
		destructionIndex++;
	}
}

