// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Materials/Material.h"
#include "Components/DestructibleComponent.h"
#include "Engine/DestructibleMesh.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeElement.generated.h"

UCLASS()
class UNREALSNAKE_API ASnakeElement : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElement();
	void DestroySnakeElement();
	void FlippSize(bool state);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SETUP")
	UDestructibleComponent* destructableMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SETUP")
		UMaterial* snakeElementMaterial01;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SETUP")
		UMaterial* snakeElementMaterial02;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UStaticMeshComponent* snakeElementMesh;




	bool flippedSize = false;
	
};
