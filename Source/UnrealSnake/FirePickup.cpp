// Krystian Slawinski

#include "FirePickup.h"
#include "FireAffector.h"
#include "UObject/ConstructorHelpers.h"
#include "engine/DestructibleMesh.h"

AFirePickup::AFirePickup()
{
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ExplosionParticleAsset(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));

	if (ExplosionParticleAsset.Succeeded())
	{
		explosionParticle = ExplosionParticleAsset.Object;
	}

	static ConstructorHelpers::FObjectFinder<UDestructibleMesh> DestructableMeshAssetr(TEXT("DestructibleMesh'/Game/FirePickup_DM.FirePickup_DM'"));

	if (DestructableMeshAssetr.Succeeded())
	{
		destructableMesh->SetDestructibleMesh(DestructableMeshAssetr.Object);
	}
}

void AFirePickup::DestroyPickup(AActor* instigator)
{
	UPrimitiveComponent* instigatorRoot = instigator->GetRootPrimitiveComponent();

	UFireAffector* NewComp;

	NewComp = NewObject<UFireAffector>(instigator->GetRootComponent(), FName("Fire Affector Component"), RF_NoFlags);
	NewComp->RegisterComponent();
	particleComponent->RegisterComponent();
	NewComp->InitializeAffector(instigator->GetRootComponent());
	
	APickup::DestroyPickup(instigator);

}
