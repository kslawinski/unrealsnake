// Krystian Slawinski

#include "WallElement.h"


// Sets default values
AWallElement::AWallElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	wallMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall Mesh"));
	wallMesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(TEXT("/Engine/BasicShapes/Cube.Cube"));

	if (MeshAsset.Succeeded())
	{
		wallMesh->SetStaticMesh(MeshAsset.Object);
		//wallMesh->SetWorldScale3D(FVector(0.3f, 0.3f, 0.3f));
	}
}

// Called when the game starts or when spawned
void AWallElement::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWallElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

