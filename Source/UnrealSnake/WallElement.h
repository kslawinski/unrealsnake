// Krystian Slawinski

#pragma once
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WallElement.generated.h"

UCLASS()
class UNREALSNAKE_API AWallElement : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWallElement();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UStaticMeshComponent* wallMesh;
	
};
