// Krystian Slawinski

#include "Affector.h"
#include "Classes/GameFramework/Pawn.h"


// Sets default values for this component's properties
UAffector::UAffector()
{
	//RegisterComponentTickFunctions(true);
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;




	// ...
}

void UAffector::InitializeAffector(USceneComponent* root)
{
	actorToAffect = GetOwner(); // get the affected actor that this component sit on

	affectCounter = affectTimeSave;

	particleComponent = NewObject<UParticleSystemComponent>(root, FName("Particle Component"), RF_NoFlags);

	particleComponent->SetupAttachment(root);
	particleComponent->RegisterComponent();
	particleComponent->SetWorldScale3D(FVector(3.0f, 3.0f, 3.0f));
	if (affectorParticle)
		particleComponent->SetTemplate(affectorParticle);

	affectorLifeCounter = affectorLifeTime;


}

// Called when the game starts
void UAffector::BeginPlay()
{
	Super::BeginPlay();

	
	// ...
	
}


// Called every frame
void UAffector::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...

	affectorLifeCounter -= DeltaTime;

	
}



