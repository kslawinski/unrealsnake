// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Materials/Material.h"
#include "Runtime/Engine/Public/EngineUtils.h"


#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Snake.generated.h"


enum class ESnakeDirectionEnum : uint8
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class UNREALSNAKE_API ASnake : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASnake();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser) override;

	void MoveSnake();

	void TurnRight();
	void TurnLeft();
	void TurnUP();
	void TurnDown();

	void AddSnakeElement();
	void ResetSnake();
	bool CheckSnakeCollision(AActor* actor);
	bool EatPickup(class APickup* pickup);

	void SelfDestruction(float);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DEBUG")
		float moveTimerSave = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DEBUG")
	float moveTimer = 0.0f;

	TArray<FVector> eatenPickupsPositions;
	TArray<class ASnakeElement*> snakeElements;

	bool isDead = false;
	float snakeSpeed;

private:
	UStaticMeshComponent* SnakeMesh;
	UCameraComponent* PlayerCamera;

	ESnakeDirectionEnum snakeDirection;
	ESnakeDirectionEnum lastSnakeDirection;

	int32 destructionIndex = 0;
	float destructionCounter = 0;
	float destructionCounterSave = 0.4f;
	float health = 100.0f;
	bool readyToTurn = false;


void ResizeSnakeElementAtLocation(FVector location);

	
};
