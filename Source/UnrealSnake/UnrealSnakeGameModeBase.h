// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameUIWidget.h"

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealSnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UNREALSNAKE_API AUnrealSnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	// Sets default values for this actor's properties
	AUnrealSnakeGameModeBase();

	void SpawnPickup();
	void GenerateMap();
	void RestartMap();

	class ASnake* snakeInstance;
	APlayerController* playerController;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DEBUG")
		float moveTimerSave = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DEBUG")
		float moveTimer = 0.0f;
	float specialPickupCounter = 0.0f;
	float specialPickupCounterSave = 10.0f;

	FString GetSpecialPickupRemainingTimeString();

	class APickup* currentSpecialPickup;
	class APickup* currentPickup;

	float specialPickupSpawnCounter = 0.0f;
	float specialPickupSpawnCounterSave = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SETUP")
	AActor* mainCameraActor;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DEBUG")
	TArray<AActor*> gameObjectsList;

	virtual void InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage) override;

	class AGameUI* gameUI;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DEBUG")
	UGameUIWidget* gameUIWidgetInstance;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SETUP")
	TSubclassOf<UGameUIWidget> UIClass;
	
	UFUNCTION(BlueprintCallable, Category = "UI")
	void ShowPausePanel(bool status);

	int32 playerScore;
private:
	bool gameOver = false;


};
