// Krystian Slawinski

#pragma once
#include "Components/TextBlock.h"
#include "Blueprint/WidgetTree.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameUIWidget.generated.h"

/**
 * 
 */
UCLASS()
class UNREALSNAKE_API UGameUIWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	virtual void NativeConstruct() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SETUP")
	UTextBlock* gameOverText;
	UFUNCTION(BlueprintCallable)
	bool RestartGame();

	class AUnrealSnakeGameModeBase* gameMode;

	UFUNCTION(BlueprintCallable)
	virtual bool ShowGameOverPanel();

	void InitUI(class AUnrealSnakeGameModeBase* GameMode);
};
